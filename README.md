# Facility Booking Management System

A system that powers a facility (auditorium/turf/ground/etc.) booking app/website.
 
#### Endpoints:

- Create a facility (multiple type facility)
- Create a facility group
- Group similar facilities together for easier management (e.g. six tennis courts in the same sports complex)
- Update any facility attribute
- Set days (e.g. holidays) it will be shut/unavailable for bookings
- Update groups of facilities in one go
- Add facility to a group 
- Remove facility from a group
- View available time slots for a given facility for a particular day
- View available time slots for a given facility for a particular span of time within a day
- Query by facility group
- Create a booking
- Activate booking
- Request verification code (may be used by the user who has booked the facility)
- Validate the verification code and mark the facility as occupied and the booking as active (may be used by an employee at the facility)
- Cancel a facility booking (up to 10 minutes before the booked slot time)
- Join a waitlist if the facility is fully booked, and if there is a cancellation first waitlist confirmed
- Check available slots for multiple (e.g. 2 or 3) facilities
- Create recurring bookings (e.g. that recur every Sunday in the month, or every Tuesday in the month, or every other week in the month, etc.)
- Submit rating for a facility
- View top 5 rated facilities of a given type
- List similar facilities available in the given slot and place
- View most booked facility 
- View most cancelled facility 
- View most unused facility
- Check peak booking hours
- Put a facility in maintenance for a specified slot
- Export the facility data as a CSV/XLSX/JSON file

## Project status
Development-in-progress.
<br>

## Installation 
1. Clone the repository
2. CD into the repository folder
3. Open the terminal and run `cabal update`
4. Create a PostgreSQL database named **appDb**
5. Connect to **appDb** database
5. Run the SQL commands in **src/DB/Commands.sql** file in **appDb** to create the necessary tables
6. Run `cabal run facility-booking-app` on the terminal

## Usage
For this, please refer to the [usage.md](https://gitlab.com/meghavx/facility-booking-app/-/blob/main/usage.md#add-a-facility) file.
