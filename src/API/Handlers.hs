{-# LANGUAGE RecordWildCards   #-}
{-# LANGUAGE OverloadedStrings #-}

module API.Handlers where

import Servant
import Database.Beam
import Data.Int (Int32)
import Data.Char (toUpper)
import Data.Foldable (for_)
import Data.Maybe (fromMaybe)
import Database.Beam.Postgres
import Data.Time (getCurrentTime)
import Data.Text (Text, pack, unpack)

import DB.Connection (connectDb)
import qualified API.Types as Ty
import qualified DB.Tables as Tb

rootHandler :: Handler Text
rootHandler = pure "Check out http://localhost:8088/<endpoint_path>"

addFacilityHandler :: Ty.Facility -> Handler Text
addFacilityHandler Ty.Facility {..} = do
  currentTime <- liftIO getCurrentTime
  liftIO $ connectDb $ \conn ->
    runBeamPostgres conn $ runInsert $ 
      insert (Tb._facilities Tb.appDb) $ 
        insertExpressions [
          Tb.Facility
            default_ 
            (val_ facility_name)
            (val_ opening_hour)
            (val_ closing_hour)
            (val_ min_slot_duration)
            (val_ slot_booking_charge) 
            (val_ currentTime)
            (val_ currentTime)
        ]
  pure $ (pack . map toUpper . unpack) facility_name <> " facility added"

deleteFacilityHandler :: Int32 -> Handler Text
deleteFacilityHandler facilityId = do
  liftIO $ connectDb $ \conn ->
    runBeamPostgres conn $ runDelete $
      delete
        (Tb._facilities Tb.appDb) 
       (\facility -> Tb._facility_id facility ==. val_ (fromIntegral facilityId)) 
  pure $ "Facility #" <> (pack . show) facilityId <> " deleted"

createGroupHandler :: Ty.FacilityGroup -> Handler Text
createGroupHandler Ty.Group {..} = do
  currentTime <- liftIO getCurrentTime
  liftIO $ connectDb $ \conn ->
    runBeamPostgres conn $ runInsert $ 
      insert (Tb._groups Tb.appDb) $
        insertExpressions [
          Tb.Group
            default_
            (val_ group_name)
            (val_ currentTime)
            (val_ currentTime)
        ]
  pure $ (pack . map toUpper . unpack) group_name <> " group created"

deleteGroupHandler :: Int32 -> Handler Text
deleteGroupHandler groupId = do
  liftIO $ connectDb $ \conn ->
    runBeamPostgres conn $ runDelete $
      delete
        (Tb._groups Tb.appDb) 
        (\group -> Tb._group_id group ==. val_ (fromIntegral groupId)) 
  pure $ "Group #" <> (pack . show) groupId <> " deleted"

addFacilitiesToGroupHandler :: Ty.FacilityToGroupMapping -> Handler Text
addFacilitiesToGroupHandler Ty.Mapping {..} = do
  currentTime <- liftIO getCurrentTime
  liftIO $ connectDb $ \conn ->
    for_ facility_ids $ \facilityId -> do
      runBeamPostgres conn $ runInsert $ 
        insert (Tb._facility_to_group_mapping Tb.appDb) $
          insertExpressions [
            Tb.FacilityToGroupMapping
              (val_ facilityId)
              (val_ group_id)
              (val_ currentTime)
              (val_ currentTime)
          ]
  pure $ "Facility ids in list -> " 
    <> (pack . show) facility_ids 
    <> " grouped under Group #" 
    <> (pack . show) group_id 

removeFacilitiesFromGroupHandler :: Ty.FacilityToGroupMapping -> Handler Text
removeFacilitiesFromGroupHandler Ty.Mapping {..} = do
  liftIO $ connectDb $ \conn ->
    for_ facility_ids $ \facilityId -> do
      runBeamPostgres conn $ runDelete $
        delete
          (Tb._facility_to_group_mapping Tb.appDb) 
          (\mapping -> Tb._m_facility_id mapping ==. val_ (fromIntegral facilityId)) 
  pure $ "Facility ids in list -> " 
    <> (pack . show) facility_ids 
    <> " removed from Group #" 
    <> (pack . show) group_id 

setHolidaysHandler :: Ty.FacilityHolidays -> Handler Text
setHolidaysHandler Ty.Holidays {..} = do
  currentTime <- liftIO getCurrentTime
  liftIO $ connectDb $ \conn ->
    for_ (zip holiday_dates holiday_descriptions) $ \(day, desc) -> do
      runBeamPostgres conn $ runInsert $ 
        insert (Tb._holidays Tb.appDb) $
          insertExpressions [
            Tb.Holiday
              (default_)
              (val_ h_facility_id)
              (val_ day)
              (val_ $ fromMaybe "" desc)
              (val_ currentTime)
              (val_ currentTime)
          ]
  pure $ "Dates in list -> " 
      <> (pack . show) holiday_dates 
      <> " set as holidays for Facility #" 
      <> (pack . show) h_facility_id 
  
getAllFacilitiesHandler :: Handler [Tb.Facility]
getAllFacilitiesHandler = do 
  let allFacilities = all_ (Tb._facilities Tb.appDb)
  liftIO $ connectDb $ \conn ->
    runBeamPostgres conn $ do
      facilities <- runSelectReturningList $ select allFacilities
      pure facilities

getFacilityByIdHandler :: Int32 -> Handler Tb.Facility
getFacilityByIdHandler facilityId = do
  maybeFacility <- liftIO $ connectDb $ \conn ->
    runBeamPostgres conn $ runSelectReturningOne $ 
      select $ do
        facility <- all_ (Tb._facilities Tb.appDb)
        guard_ (Tb._facility_id facility ==. val_ (fromIntegral facilityId))
        pure facility
  case maybeFacility of
    Just facility -> pure facility
    Nothing -> throwError err404 { errBody = "Facility by the provided id not found" }

putFacilityUnderMaintenanceHandler :: Ty.FacilityMaintenanceHours -> Handler Text
putFacilityUnderMaintenanceHandler Ty.MaintenanceHours {..} = do
  currentTime <- liftIO getCurrentTime
  liftIO $ connectDb $ \conn ->
    runBeamPostgres conn $ runInsert $ 
      insert (Tb._maintenance_hours Tb.appDb) $
        insertExpressions [
          Tb.MaintenanceHours
            (default_)
            (val_ mh_facility_id)
            (val_ mh_start_date)
            (val_ mh_end_date)
            (val_ mh_start_time)
            (val_ mh_end_time)
            (val_ currentTime)
            (val_ currentTime)
        ]
  pure $ "Facility #"
      <> (pack . show) mh_facility_id 
      <> " maintenance duration set between "
      <> (pack . show) mh_start_date
      <> " "
      <> (pack . show) mh_start_time
      <> ":00 Hrs and "
      <> (pack . show) mh_end_date
      <> " "
      <> (pack . show) mh_end_time
      <> ":00 Hrs"
     