{-# LANGUAGE DeriveGeneric  #-}
{-# LANGUAGE DeriveAnyClass #-}

module API.Types where

import Data.Text (Text)
import Data.Int (Int32)
import GHC.Generics (Generic)
import Data.Time.Calendar (Day)
import Data.Aeson (FromJSON, ToJSON)

data Facility = 
  Facility
  { facility_name       :: Text
  , opening_hour        :: Int32
  , closing_hour        :: Int32
  , min_slot_duration   :: Int32
  , slot_booking_charge :: Int32 
  }
  deriving (Generic, FromJSON, ToJSON, Show)

data FacilityGroup = 
  Group 
  { group_name  :: Text 
  } 
  deriving (Generic, FromJSON, ToJSON, Show)

data FacilityToGroupMapping = 
  Mapping 
  { facility_ids :: [Int32]
  , group_id     :: Int32
  }
  deriving (Generic, FromJSON, ToJSON, Show)

data FacilityHolidays = 
  Holidays
  { h_facility_id        :: Int32
  , holiday_dates        :: [Day]
  , holiday_descriptions :: [Maybe Text]
  }
  deriving (Generic, FromJSON, ToJSON, Show)

data FacilityMaintenanceHours = 
  MaintenanceHours
  { mh_facility_id :: Int32
  , mh_start_date  :: Day
  , mh_end_date    :: Day
  , mh_start_time  :: Int32
  , mh_end_time    :: Int32
  }
  deriving (Generic, FromJSON, ToJSON, Show)