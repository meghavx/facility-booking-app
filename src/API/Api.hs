{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE TypeApplications  #-}
{-# LANGUAGE OverloadedStrings #-}

module API.Api (app) where

import Servant
import Data.Text (Text)
import Data.Int (Int32)

import qualified DB.Tables as Tb
import qualified API.Types as Ty
import qualified API.Handlers as H

app :: Application
app = serve (Proxy @API) appServer

type API = 
  Root 
  :<|> AddFacility
  :<|> DeleteFacility 
  :<|> CreateGroup
  :<|> DeleteGroup
  :<|> AddFacilitiesToGroup
  :<|> RemoveFacilitiesFromGroup
  :<|> SetHolidays
  :<|> GetAllFacilities
  :<|> GetFacilityById
  :<|> PutFacilityUnderMaintenance

appServer :: Server API
appServer =
  H.rootHandler
  :<|> H.addFacilityHandler 
  :<|> H.deleteFacilityHandler
  :<|> H.createGroupHandler 
  :<|> H.deleteGroupHandler
  :<|> H.addFacilitiesToGroupHandler
  :<|> H.removeFacilitiesFromGroupHandler
  :<|> H.setHolidaysHandler
  :<|> H.getAllFacilitiesHandler
  :<|> H.getFacilityByIdHandler
  :<|> H.putFacilityUnderMaintenanceHandler
  
type Root =
  Get '[JSON] Text

type AddFacility =
  "admin"
  :> "add_facility" 
  :> ReqBody '[JSON] Ty.Facility 
  :> Post '[JSON] Text

type DeleteFacility = 
  "admin"
  :> "delete_facility" 
  :> Capture "facility_id" Int32 
  :> Delete '[JSON] Text

type CreateGroup = 
  "admin"
  :> "create_group"
  :> ReqBody '[JSON] Ty.FacilityGroup
  :> Post '[JSON] Text

type DeleteGroup = 
  "admin"
  :> "delete_group"
  :> Capture "group_id" Int32
  :> Delete '[JSON] Text

type AddFacilitiesToGroup =
  "admin"
  :> "add_facilities_to_group" 
  :> ReqBody '[JSON] Ty.FacilityToGroupMapping 
  :> Post '[JSON] Text

type RemoveFacilitiesFromGroup =
  "admin"
  :> "remove_facilities_from_group" 
  :> ReqBody '[JSON] Ty.FacilityToGroupMapping 
  :> Post '[JSON] Text

type SetHolidays = 
  "admin"
  :> "set_holidays"
  :> ReqBody '[JSON] Ty.FacilityHolidays 
  :> Post '[JSON] Text

type GetAllFacilities =
  "admin"
  :> "facilities"
  :> Get '[JSON] [Tb.Facility] 

type GetFacilityById =
  "admin"
  :> "facility"
  :> Capture "facility_id" Int32
  :> Get '[JSON] Tb.Facility 

type PutFacilityUnderMaintenance =
  "admin"
  :> "put_facility_under_maintenance"
  :> ReqBody '[JSON] Ty.FacilityMaintenanceHours 
  :> Post '[JSON] Text