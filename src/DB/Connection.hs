module DB.Connection (connectDb) where

import Database.PostgreSQL.Simple

connectDb :: (Connection -> IO c) -> IO c
connectDb = withConnect connectInfo
  
connectInfo :: ConnectInfo
connectInfo = defaultConnectInfo {
    connectHost     = "127.0.0.1",
    connectPort     =  5432,
    connectUser     = "meghav",
    connectPassword = "1234",
    connectDatabase = "appDb"
  }