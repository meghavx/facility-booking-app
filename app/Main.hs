module Main where

import API.Api (app)
import Network.Wai.Handler.Warp (run)

main :: IO ()
main = do
  putStrLn "App is running at http://localhost:8088/"    
  run 8088 app
