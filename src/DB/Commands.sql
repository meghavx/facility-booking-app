CREATE TYPE ROLE AS ENUM (
  'Admin', 
  'EndUser'
);

CREATE TYPE SLOT_STATUS AS ENUM (
  'Available', 
  'Booked', 
  'Under_Maintenance',
  'Unavailable'
);

CREATE TYPE BOOKING_STATUS AS ENUM (
  'Active',
  'Cancelled',
  'WaitListed'
);

CREATE TABLE Users (
  user_id             BIGSERIAL PRIMARY KEY,
  user_name           VARCHAR(50) NOT NULL,
  user_email          VARCHAR(150) NOT NULL,
  user_phone_number   INTEGER NOT NULL,
  user_password       VARCHAR(150),
  user_role           ROLE NOT NULL,
  user_added_at      TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  user_updated_at     TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Facilities (
  facility_id         SERIAL PRIMARY KEY, 
  facility_name       TEXT NOT NULL,
  opening_hour        INTEGER NOT NULL,
  closing_hour        INTEGER NOT NULL,
  min_slot_duration   INTEGER NOT NULL,
  slot_booking_charge INTEGER NOT NULL,
  facility_added_at   TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  facility_updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Groups (
  group_id            SERIAL PRIMARY KEY,
  group_name          TEXT NOT NULL,
  group_added_at      TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  group_updated_at    TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE Facility_To_Group_Mapping (
  m_facility_id       INTEGER PRIMARY KEY,
  m_group_id          INTEGER,
  m_added_at          TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  m_updated_at        TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,

  FOREIGN KEY(m_facility_id) REFERENCES Facilities(facility_id) ON DELETE CASCADE,
  FOREIGN KEY(m_group_id) REFERENCES Groups(group_id) ON DELETE CASCADE
);

CREATE TABLE Holidays (
  holiday_id          SERIAL PRIMARY KEY,
  h_facility_id       INTEGER NOT NULL,
  holiday_date        DATE NOT NULL,
  holiday_description TEXT,
  holiday_added_at    TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  holiday_updated_at  TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  UNIQUE (h_facility_id, holiday_date)
);

CREATE TABLE Bookings (
  booking_id          SERIAL PRIMARY KEY,
  b_user_id           INTEGER NOT NULL,
  b_facility_id       INTEGER NOT NULL,
  slot_date           DATE NOT NULL,
  slot_start_time     INTEGER NOT NULL,
  slot_end_time       INTEGER NOT NULL,
  slot_status         SLOT_STATUS NOT NULL,
  booking_status      BOOKING_STATUS NOT NULL,
  booking_added_at    TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  booking_updated_at  TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,

  FOREIGN KEY(b_user_id) REFERENCES Users(user_id) ON DELETE CASCADE,
  FOREIGN KEY(b_facility_id) REFERENCES Facilities(facility_id) ON DELETE CASCADE
);

CREATE TABLE Ratings (
  rating_id           BIGSERIAL PRIMARY KEY,
  r_user_id           INTEGER NOT NULL,
  r_facility_id       INTEGER NOT NULL,
  rating              INTEGER NOT NULL CHECK (rating BETWEEN 1 AND 5),
  rating_added_at     TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  rating_updated_at   TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,

  FOREIGN KEY(r_user_id) REFERENCES Users(user_id) ON DELETE CASCADE,
  FOREIGN KEY(r_facility_id) REFERENCES Facilities(facility_id) ON DELETE CASCADE
);

CREATE TABLE Maintenance_Hours (
  mh_id               SERIAL PRIMARY KEY,
  mh_facility_id      INTEGER NOT NULL,
  mh_start_date       DATE NOT NULL,
  mh_end_date         DATE NOT NULL,
  mh_start_time       INTEGER NOT NULL,
  mh_end_time         INTEGER NOT NULL,
  mh_added_at         TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,
  mh_updated_at       TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP,

  FOREIGN KEY(mh_facility_id) REFERENCES Facilities(facility_id) ON DELETE CASCADE
);