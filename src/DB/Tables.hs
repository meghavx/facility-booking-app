{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeSynonymInstances  #-}

module DB.Tables where

import Database.Beam
import Data.Text (Text)
import Data.Int (Int32)
import Data.Time (UTCTime)
import Data.Aeson (ToJSON)
import GHC.Generics (Generic)
import Database.Beam.Postgres
import Data.Time.Calendar (Day)
import Database.Beam.Backend.SQL.BeamExtensions (SqlSerial)

-- Table type for Facilities
data FacilityT f = 
  Facility
  { _facility_id         :: C f (SqlSerial Int32)
  , _facility_name       :: C f Text
  , _opening_hour        :: C f Int32
  , _closing_hour        :: C f Int32
  , _min_slot_duration   :: C f Int32
  , _slot_booking_charge :: C f Int32
  , _facility_added_at   :: C f UTCTime
  , _facility_updated_at :: C f UTCTime 
  }
  deriving (Generic, Beamable)

type Facility = FacilityT Identity
deriving instance Show Facility
deriving instance ToJSON Facility

type FacilityId = PrimaryKey FacilityT Identity

instance Table FacilityT where
  data PrimaryKey FacilityT f = FacilityId (C f (SqlSerial Int32)) 
    deriving (Generic, Beamable)
  primaryKey = FacilityId . _facility_id


-- Table type for Groups (of Facilities)
data GroupT f = 
  Group
  { _group_id         :: C f (SqlSerial Int32)
  , _group_name       :: C f Text
  , _group_added_at   :: C f UTCTime
  , _group_updated_at :: C f UTCTime 
  } 
  deriving (Generic, Beamable)

type Group = GroupT Identity
deriving instance Show Group
deriving instance ToJSON Group

type GroupId = PrimaryKey GroupT Identity

instance Table GroupT where
  data PrimaryKey GroupT f = GroupId (C f (SqlSerial Int32)) 
    deriving (Generic, Beamable)
  primaryKey = GroupId . _group_id


-- Table type for mapping Facility to a Group (many-to-one mapping)
data FacilityToGroupMappingT f = 
  FacilityToGroupMapping
  { _m_facility_id :: C f Int32
  , _m_group_id    :: C f Int32
  , _m_added_at    :: C f UTCTime
  , _m_updated_at  :: C f UTCTime 
  } 
  deriving (Generic, Beamable)

type FacilityToGroupMapping = FacilityToGroupMappingT Identity
deriving instance Show FacilityToGroupMapping

type MappingId = PrimaryKey FacilityToGroupMappingT Identity

instance Table FacilityToGroupMappingT where
  data PrimaryKey FacilityToGroupMappingT f = MappingId (C f Int32) 
    deriving (Generic, Beamable)
  primaryKey = MappingId . _m_facility_id


-- Table type for setting a Holiday for a Facility
data HolidayT f = 
  Holiday
  { _holiday_id          :: C f (SqlSerial Int32)
  , _h_facility_id       :: C f Int32
  , _holiday_date        :: C f Day
  , _holiday_description :: C f Text
  , _holiday_added_at    :: C f UTCTime
  , _holiday_updated_at  :: C f UTCTime 
  } 
  deriving (Generic, Beamable)

type Holiday = HolidayT Identity
deriving instance Show Holiday

type HolidayId = PrimaryKey HolidayT Identity

instance Table HolidayT where
  data PrimaryKey HolidayT f = HolidayId (C f (SqlSerial Int32)) 
    deriving (Generic, Beamable)
  primaryKey = HolidayId . _holiday_id


-- Table type for putting a Facility Under Maintenance For a Specified Duration
data MaintenanceHoursT f =
  MaintenanceHours
  { _mh_id          :: C f (SqlSerial Int32)
  , _mh_facility_id :: C f Int32
  , _mh_start_date  :: C f Day
  , _mh_end_date    :: C f Day
  , _mh_start_time  :: C f Int32
  , _mh_end_time    :: C f Int32
  , _mh_added_at    :: C f UTCTime
  , _mh_updated_at  :: C f UTCTime 
  }
  deriving (Generic, Beamable)

type MaintenanceHours = MaintenanceHoursT Identity
deriving instance Show MaintenanceHours

type MaintenanceHoursId = PrimaryKey MaintenanceHoursT Identity

instance Table MaintenanceHoursT where
  data PrimaryKey MaintenanceHoursT f = MaintenanceHoursId (C f (SqlSerial Int32)) 
    deriving (Generic, Beamable)
  primaryKey = MaintenanceHoursId . _mh_id


-- Definition of the database
data AppDb f = 
  AppDb
  { _facilities                :: f (TableEntity FacilityT) 
  , _groups                    :: f (TableEntity GroupT)
  , _facility_to_group_mapping :: f (TableEntity FacilityToGroupMappingT)
  , _holidays                  :: f (TableEntity HolidayT) 
  , _maintenance_hours         :: f (TableEntity MaintenanceHoursT)
  }
  deriving (Generic, Database Postgres)


-- Description of the database
appDb :: DatabaseSettings Postgres AppDb
appDb = defaultDbSettings
