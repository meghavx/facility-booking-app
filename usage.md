## Add a facility 
```
Method: 
POST

Endpoint path:
http://localhost:8088/admin/add_facility/

Request Body:
raw | JSON

{ 
  "facility_name" : "Court 1",
  "opening_hour" : 5,
  "closing_hour" : 20,
  "min_slot_duration" : 1,
  "slot_booking_charge" : 200
}

Response Body:
Text

"COURT 1 facility added"
```

## Delete a facility
```
Method: 
DELETE

Endpoint path:
http://localhost:8088/admin/delete_facility/<facility_id>

Response Body:
Text

"Facility #<facility_id> deleted"
```

## Create a group 
```
Method: 
POST

Endpoint path:
http://localhost:8088/admin/create_group/

Request Body:
raw | JSON

{ 
  "group_name" : "XYZ Sports Complex"
}

Response Body:
Text

"XYZ SPORTS COMPLEX group created"
```

## Delete a group 
```
Method: 
DELETE

Endpoint path:
http://localhost:8088/admin/delete_group/<group_id>

Response Body:
Text

"Group #<group_id> deleted"
```

## Add facilities to a group 
```
Method: 
POST

Endpoint path:
http://localhost:8088/admin/add_facilities_to_group/

Request Body:
raw | JSON

{ 
  "facility_ids" : [1,2,3],
  "group_id"     : 1
}

Response Body:
Text

"Facility ids in list -> [1,2,3] grouped under Group #1"
```

## Remove facilities from a group
```
Method: 
POST

Endpoint path:
http://localhost:8088/admin/remove_facilities_from_group/

Request Body:
raw | JSON

{ 
  "facility_ids" : [3],
  "group_id"     : 1
}

Response Body:
Text

"Facility ids in list -> [3] removed from Group #1"
```

## Set Holidays for a facility 
```
Method: 
POST

Endpoint path:
http://localhost:8088/admin/set_holidays/

Request Body:
raw | JSON

{ 
  "h_facility_id" : 1,
  "holiday_dates" : ["2024-06-20", "2024-08-15"],
  "holiday_descriptions" : [null, "Independence Day"] 
}

Response Body:
Text

"Dates in list -> [2024-06-20,2024-08-15] set as holidays for Facility #1"
```

## Get all facilities data 
```
Method: 
GET

Endpoint path:
http://localhost:8088/admin/facilities/

Response Body:
[
    {
        "_closing_hour": 20,
        "_facility_created_at": "2024-06-18T19:12:39.936169Z",
        "_facility_id": 1,
        "_facility_name": "Court 1",
        "_facility_updated_at": "2024-06-18T19:12:39.936169Z",
        "_min_slot_duration": 1,
        "_opening_hour": 5,
        "_slot_booking_charge": 200
    },
    {
        "_closing_hour": 20,
        "_facility_created_at": "2024-06-18T19:12:59.88952Z",
        "_facility_id": 2,
        "_facility_name": "Court 2",
        "_facility_updated_at": "2024-06-18T19:12:59.88952Z",
        "_min_slot_duration": 1,
        "_opening_hour": 5,
        "_slot_booking_charge": 149
    },
    {
        "_closing_hour": 23,
        "_facility_created_at": "2024-06-19T07:18:01.162499Z",
        "_facility_id": 4,
        "_facility_name": "Silver Hall",
        "_facility_updated_at": "2024-06-19T07:18:01.162499Z",
        "_min_slot_duration": 4,
        "_opening_hour": 11,
        "_slot_booking_charge": 250000
    },
    {
        "_closing_hour": 0,
        "_facility_created_at": "2024-06-19T07:18:51.416099Z",
        "_facility_id": 6,
        "_facility_name": "Platinum Hall",
        "_facility_updated_at": "2024-06-19T07:18:51.416099Z",
        "_min_slot_duration": 5,
        "_opening_hour": 11,
        "_slot_booking_charge": 500000
    }
]
```

## Get facility data by facility id
```
Method: 
GET

Endpoint path:
http://localhost:8088/admin/facility/<facility_id>

Response Body:
{
    "_closing_hour": 20,
    "_facility_created_at": "2024-06-18T19:12:59.88952Z",
    "_facility_id": 2,
    "_facility_name": "Court 2",
    "_facility_updated_at": "2024-06-18T19:12:59.88952Z",
    "_min_slot_duration": 1,
    "_opening_hour": 5,
    "_slot_booking_charge": 149
}
```

## Put a facility under maintenance for a specified duration
```
Method: 
POST

Endpoint path:
http://localhost:8088/admin/put_facility_under_maintenance/

Request Body:
raw | JSON

{ 
  "mh_facility_id" : 1,
  "mh_start_date" : "2024-06-25",
  "mh_end_date" : "2024-06-25",
  "mh_start_time" : 13,
  "mh_end_time" : 17
}

Response Body:
Text

"Facility #1 maintenance duration set between 2024-06-21 8:00 Hrs and 2024-06-21 10:00 Hrs"
```

### Notes:
- Please follow 24-hour time notation and only specify whole hours.
- Refer to the following examples to see valid and invalid time inputs:

  <br> Valid inputs for time (in hours):
    - 8 for 8 AM
    - 13 for 1 PM
    - 0 for 12 AM
    - 12 for 12 PM

  <br>Invalid inputs for time (in hours):
    - 8.5 for 8:30 AM
    - 8.30 for 8:30 AM 
